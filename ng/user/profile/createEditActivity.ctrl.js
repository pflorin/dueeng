angular.module('app').controller('creteEditActivityCtrl', function ($rootScope, $scope, $modal, $log) {

  $rootScope.createEditActivity = function (isCreate, activity) {
    
    var modalInstance = $modal.open({
      animation: false,
      templateUrl: './user/profile/create_edit_activity.html',
      controller: 'ModalInstanceCtrl',
      resolve: {
        items: function () {
          return $scope.items
        },
        isCreate: function() {
          return isCreate
        },
        activity: function() {
          return activity
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
    }, function () {
    });
  };

});

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

angular.module('app').controller('ModalInstanceCtrl', function ($rootScope, $scope, $modalInstance, items, isCreate, UserSvc, activity, ActivitiesSvc) {

  $scope.isCreate = isCreate
  var addObj
  if (isCreate) {
    addObj = {};
  } else {
    addObj = activity
  }

  /*$scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };*/

  $scope.validate = function() {
    if (!$scope.what || $scope.what == '') {
      $scope.hasError = true
      $scope.errorMessage = 'Please fill what is this about.'
      return false;
    }

    if ($scope.seeSelectOption == 'Select friends'
        && !$scope.whoWillSeeItems.length) {
      $scope.errorMessage = 'Please select at least one friend on "Who will see" part'
      $scope.hasError = true
      return false
    }

    return true

  }

  $scope.isSaving = false
  $scope.hasError = false
  $scope.hasSuccess = false
  $scope.save = function () {
    $scope.isSaving = true
    $scope.hasError = false
    $scope.hasSuccess = false
    if (!$scope.validate()) {
      $scope.isSaving = false
      return
    }

    addObj.what = $scope.what;
    addObj.how = $scope.how;
    addObj.isDone = $scope.isDone;
    addObj.visibility = $scope.seeSelectOption;

    ActivitiesSvc.addEditActivity(addObj, $scope.isCreate)
    .then(function(val) {
      if (val && val.data) {
        $scope.isSaving = false
        if (val.data.status == 'validationError') {
          $scope.hasError = true
          $scope.errorMessage = 'Plase try again or hit refresh.'
          return
        }

        if (val.data.status == 'success') {
          $scope.hasSuccess = true

          if ($scope.isCreate == true) {
            $scope.isCreate = false
            addObj._id = val.data.id;
            $scope.successMessage = 'Created!'
          } else {
            addObj.lastModified = val.data.lastModified;
            $scope.successMessage = 'Saved!'
          }
          return
        }
        $scope.hasError = true
        $scope.errorMessage = 'Plase try again or hit refresh.'
        return 
      }
      
    }, function(err) {
      $scope.isSaving = false
      $scope.hasError = true
      $scope.errorMessage = 'Plase try again or hit refresh.'
    })

  };

  $scope.close = function () {
    $modalInstance.dismiss('cancel');
  };


  $scope.seeSelectOptions = ['Public', 'All friends', 'Private']

  if ($scope.isCreate) {
    $scope.what = '';
    $scope.seeSelectOption = $scope.seeSelectOptions[1]
  } else {
    $scope.what = addObj.what;
    $scope.how = addObj.how;
    $scope.isDone = addObj.isDone;
    $scope.seeSelectOption = addObj.visibility
  }



});