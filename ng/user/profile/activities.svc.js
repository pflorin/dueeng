angular.module('app')
.service('ActivitiesSvc', function($http) {
	var svc = this

	svc.addEditActivity = function(activity, isCreate) {
		return $http.post('/api/activities/add_edit_activity', {
			activity: activity,
			isCreate: isCreate
		});
	}

	svc.deleteActivity = function(id) {
		return $http.post('/api/activities/delete_activity', {
			id: id
		});
	}

	svc.find = function(currentState, lastDateModified, seeingUser) {

		if (currentState == 'main.watchedActivities') {
			return $http.post('/api/activities/find_watched_activities', {
				lastDateModified: lastDateModified
			}).then(function(val) {
				return val.data
			}, function(error) {
				return null
			});
		} else if (currentState == 'main.lastActivities') {
			return $http.post('/api/activities/find_last_activities', {
				lastDateModified: lastDateModified
			}).then(function(val) {
				return val.data
			}, function(error) {
				return null
			});

		} else if (currentState == 'main.activities') {

			return $http.post('/api/activities/find', {
				lastDateModified: lastDateModified,
				seeingUser: seeingUser._id
			}).then(function(val) {
				return val.data
			}, function(error) {
				return null
			});
		} 


	}

	svc.goodLuck = function(id, op) {
		return $http.post('/api/activities/good_luck', {
			id: id,
			op: op
		});
	}

	svc.congrats = function(id, op) {
		return $http.post('/api/activities/congrats', {
			id: id,
			op: op
		});
	}

	svc.watch = function(id, op) {
		return $http.post('/api/activities/watch', {
			id: id,
			op: op
		});
	}
})