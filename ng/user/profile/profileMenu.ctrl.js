angular.module('app').controller('ProfileMenuCtrl', function($rootScope, $scope, $state, $stateParams, seeingUser, friends) {

	if(seeingUser == null) {
		$state.go('main.watchedActivities', {}, {reload: true});
	}

	$rootScope.seeingUsername = $stateParams.username;
	if (seeingUser && seeingUser != '') {
		$rootScope.seeingUser = seeingUser
	}
	if (friends && friends != '') {
		$rootScope.friends = friends
	} else {
		$rootScope.friends = []
	}
	$scope.currentClick = null
	$scope.goTo = function(state) {
		if (state == 'activities') {
			$scope.currentClick = 'activities'
			$state.go('main.activities', {username: $stateParams.username}, {reload: true});
		} else if (state == 'friends') {
			$scope.currentClick = 'friends'
			$state.go('main.friends', {username: $stateParams.username}, {reload: true});
		} else if (state == 'about') {
			$scope.currentClick = 'about'
			$state.go('main.about', {username: $stateParams.username}, {reload: true});
		} else if (state == 'myProfile') {
			$state.go('main.activities', {username: $rootScope.currentUser.username}, {reload: true});
		} else if (state == 'findPeople') {
			$state.go('main.findPeople', {}, {reload: true});
		}else if (state == 'watchedActivities') {
			$state.go('main.watchedActivities', {}, {reload: true});
		}else if (state == 'lastActivities') {
			$state.go('main.lastActivities', {}, {reload: true});
		}else if (state == 'watchedPeople') {
			$state.go('main.watchedPeople', {}, {reload: true});
		}else if (state == 'friendRequests') {
			$state.go('main.friendRequests', {}, {reload: true});
		}
		return
	}
})