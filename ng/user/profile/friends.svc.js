angular.module('app')
.service('FriendsSvc', function($http) {
	var svc = this

	svc.find = function(state, keywords, lastUsername, seeingUser) {
		if (state == 'main.findPeople') {
			return svc.findPeople(keywords, lastUsername);
		} else if (state == 'main.friends') {
			return svc.getFriendsForProfile(keywords, lastUsername, seeingUser);
		} else if (state == 'main.watchedPeople') {
			return svc.getWatchedPeople(keywords, lastUsername, seeingUser);
		} else if (state == 'main.friendRequests') {
			return svc.getFriendRequests(keywords, lastUsername, seeingUser);
		}
	}

	svc.findPeople = function(keywords, lastUsername) {
		return $http.post('/api/friends/find_people', {
			keywords: keywords,
			lastUsername: lastUsername
		}).then(function(val) {
			return val.data
		}, function(error) {
			return null
		})
	}

	svc.getWatchedPeople = function(keywords, lastUsername) {
		return $http.post('/api/friends/watched_people', {
			keywords: keywords,
			lastUsername: lastUsername
		}).then(function(val) {
			return val.data
		}, function(error) {
			return null
		})
	}

	svc.getFriendRequests = function(keywords, lastUsername) {
		return $http.post('/api/friends/friend_requests', {
			keywords: keywords,
			lastUsername: lastUsername
		}).then(function(val) {
			return val.data
		}, function(error) {
			return null
		})
	}

	svc.getFriendsForProfile = function(keywords, lastUsername, seeingUser) {
		return $http.post('/api/friends/get_friends_for_profile', {
			keywords: keywords,
			lastUsername: lastUsername,
			seeingUserId: seeingUser._id
		}).then(function(val) {
			return val.data
		}, function(error) {
			return null
		})
	}

	svc.getFriends = function() {
		return $http.post('/api/friends/get_friends', {
			data: ''
		}).then(function(val) {
			return val.data
		}, function(error) {
			return null
		})
	}

	svc.sendFriendRequest = function(id1, id2) {
		return $http.post('/api/friends/send_friend_request', {
			id1: id1,
			id2: id2
		})
	}

	svc.undoSendFriendRequest = function(id) {
		return $http.post('/api/friends/undo_send_friend_request', {
			id: id
		})
	}

	svc.acceptFriendRequest = function(id) {
		return $http.post('/api/friends/accept_friend_request', {
			id: id
		})
	}

	svc.unfriend = function(id) {
		return $http.post('/api/friends/unfriend', {
			id: id
		})
	}

	svc.watch = function(id, op) {
		return $http.post('/api/friends/watch', {
			id: id,
			op: op
		});
	}
})