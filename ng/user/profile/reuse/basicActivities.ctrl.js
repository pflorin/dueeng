angular.module('app').controller('BasicActivitiesCtrl', function($rootScope, $scope, $state, $stateParams, ActivitiesSvc) {

	$scope.startLoadingActivities = true;

	$scope.noActivities = false;
	$scope.loadMore = false
	$scope.hasError = false
	$scope.loading = false
	$scope.searchedActivities = []
	$scope.lastDateModified = null;
	$scope.errorMessage = "There was an error. Please try again!"

	$scope.loadMoreActivities = function() {
		$scope.loading = true
		$scope.hasError = false
		$scope.searchedActivities.length > 0 && ($scope.lastDateModified = $scope.searchedActivities[$scope.searchedActivities.length - 1].lastModified)
		ActivitiesSvc.find($scope.currentState, $scope.lastDateModified, $rootScope.seeingUser)
		.then(function(response) {
			$scope.loading = false
			if (response) {
				if (response == 'invalid_auth') {
					$scope.$emit('invalid_auth')
					return;
				}
				if (!response.activities.length) {
					$scope.noActivities = true;
					return;
				} 
				$scope.searchedActivities = $scope.searchedActivities.concat(response.activities)
				if (response.activities.length == 10) {
					$scope.loadMore = true
				} else {
					$scope.loadMore = false
				}
			} else {
				$scope.hasError = true
			}
		})
	}

	$scope.deleteActivity = function (activity) {
    
	    activity.isDeleting = true;
	    ActivitiesSvc.deleteActivity(activity._id)
	    .then(function(val) {
	      activity.isDeleting = false
	      if (val && val.data) {  

	      	var index = -1;
	      	$scope.searchedActivities.forEach(function(e, i) {
	      		if (e._id == activity._id) {
	      			index = i;
	      		}
	      	})
	      	if (index != -1) {
	      		$scope.searchedActivities.splice(index, 1);
	      	}
	      	if (!$scope.searchedActivities.length) {
					$scope.noActivities = true;
			} 
	        return 
	      }
	      activity.errorMessage = 'Plase try again or hit refresh.'
	    }, function(err) {
	      activity.isDeleting = false
	      activity.errorMessage = 'Plase try again or hit refresh.'
	    })

  	};

	if ($scope.startLoadingActivities) {
		$scope.loadMoreActivities($scope.lastDateModified)
	}

	$scope.goodLuck = function(activity) {
		if (!activity.goodLuckIds) {
			activity.goodLuckIds = [];
		} 

		if (activity.goodLuckIds.indexOf($rootScope.currentUser._id) == -1) {
			activity.goodLuckIds.push($rootScope.currentUser._id)
			ActivitiesSvc.goodLuck(activity._id, 1)
		} else {
			activity.goodLuckIds.splice(activity.goodLuckIds.indexOf($rootScope.currentUser._id), 1);
			ActivitiesSvc.goodLuck(activity._id, 0)
		}
	}

	$scope.congrats = function(activity) {
		if (!activity.congratsIds) {
			activity.congratsIds = [];
		} 

		if (activity.congratsIds.indexOf($rootScope.currentUser._id) == -1) {
			activity.congratsIds.push($rootScope.currentUser._id)
			ActivitiesSvc.congrats(activity._id, 1)
		} else {
			activity.congratsIds.splice(activity.congratsIds.indexOf($rootScope.currentUser._id), 1);
			ActivitiesSvc.congrats(activity._id, 0)
		}
	}

	$scope.watch = function(activity) {
		if (!activity.watchIds) {
			activity.watchIds = [];
		} 

		if (activity.watchIds.indexOf($rootScope.currentUser._id) == -1) {
			activity.watchIds.push($rootScope.currentUser._id)
			ActivitiesSvc.watch(activity._id, 1)
		} else {
			activity.watchIds.splice(activity.watchIds.indexOf($rootScope.currentUser._id), 1);
			ActivitiesSvc.watch(activity._id, 0)
		}
	}

	$scope.existsIn = function(elem, array) {
		if (array) {
			if (!array.length) {
				return false;
			}
			if (array.indexOf(elem) != -1) {
				return true;
			} else {
				return false;
			}
		} else {
			return false
		}
	}
})