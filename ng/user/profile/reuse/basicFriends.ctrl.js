angular.module('app').controller('BasicFriendsCtrl', function($rootScope, $scope, $state, $stateParams, FriendsSvc) {
	$scope.loadMore = false
	$scope.hasError = false
	$scope.loading = false
	$scope.searchedFriends = []
	$scope.noFriends = false;
	$scope.noResults = false;
	//$rootScope.friends = []
	$scope.keywords = ""
	$scope.errorMessage = "There was an error. Please try again!"

	$scope.search = function(keywords) {
		$scope.searchedFriends = []
		$scope.loading = true
		$scope.hasError = false
		$scope.loadMore = false
		var lastUsername = ""
		$scope.keywords = keywords
		FriendsSvc.find($scope.currentState, keywords, lastUsername, $rootScope.seeingUser)
		.then(function(response) {
			$scope.loading = false
			if (response) {
				if (response == 'invalid_auth') {
					$scope.$emit('invalid_auth')
					return;
				}
				$rootScope.friends = response.friends
				$scope.searchedFriends = response.users
				if (!response.users.length && keywords == '') {
					$scope.noFriends = true;
				} else if (!response.users.length && keywords != ''){
					$scope.noResults = true;
				} else {
					$scope.noFriends = false;
					$scope.noResults = false;
				}
				if (response.users.length == 15) {
					$scope.loadMore = true
				} else {
					$scope.loadMore = false
				}
			} else {
				$scope.hasError = true
			}
		})
	}

	$scope.loadMoreFriends = function() {
		$scope.loading = true
		$scope.hasError = false
		if (!$scope.searchedFriends.length) {
			return;
		}
		var lastUsername = $scope.searchedFriends[$scope.searchedFriends.length - 1].username
		FriendsSvc.find($scope.currentState, $scope.keywords, lastUsername, $stateParams.username)
		.then(function(response) {
			$scope.loading = false
			if (response) {
				if (response == 'invalid_auth') {
					$scope.$emit('invalid_auth')
					return;
				}
				$rootScope.friends = response.friends
				$scope.searchedFriends = $scope.searchedFriends.concat(response.users)
				if (response.users.length == 15) {
					$scope.loadMore = true
				} else {
					$scope.loadMore = false
				}
			} else {
				$scope.hasError = true
			}
		})
	}

	if ($scope.startLoadingFriends) {
		$scope.search('')
	}
})