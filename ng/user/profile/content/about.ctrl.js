angular.module('app').controller('AboutCtrl', function($scope, $rootScope, UserSvc) {

	$scope.isEditing = false
	$scope.isLoading = false
	$scope.isError = false;
	$scope.about = $rootScope.seeingUser.about
	$scope.edit = function() {
		$scope.isEditing = true
		$scope.isLoading = false
	}

	$scope.save = function() {

		$scope.isLoading = true
		$scope.isError = false
		UserSvc.updateAbout($scope.about)
		.then(function(response) {
			$scope.isLoading = false
			$scope.isEditing = false
			if (response) {
				if (response == 'invalid_auth') {
					$scope.$emit('invalid_auth')
					return;
				}
				if (response.status == 'success') {
					$rootScope.seeingUser.about = $scope.about
				} else {
					$scope.isError = true
					$scope.about = $rootScope.seeingUser.about
				}
			} else {
				$scope.isError = true			
				$scope.about = $rootScope.seeingUser.about
			}
		})	
	}

	$scope.cancel = function() {
		$scope.isEditing = false
		$scope.isError = false
		$scope.isLoading = false
		$scope.about = $rootScope.seeingUser.about
	}
})