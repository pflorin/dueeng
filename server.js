var express = require('express')
var bodyParser = require('body-parser')
var statics = require('./statics')


var app = express()
app.use(bodyParser.json())

app.use(require('./auth'))

app.use('/api/sessions', require('./controllers/api/sessions'))
app.use('/api/users', require('./controllers/api/users'))
app.use('/api/friends', require('./controllers/api/user/profile/friends'))
app.use('/api/activities', require('./controllers/api/user/profile/activities'))

app.use(require('./controllers/static'))

app.all('/*', function(req, res, next) {
    // Just send the index.html for other files to support HTML5Mode
    res.sendFile(statics.MAIN_LAYOUT_FILE, { root: __dirname });
});

var port = process.env.PORT || 3000
var server = app.listen(port, function() {
	console.log('Server is up! on', port)
})