var router = require('express').Router()
var User = require('../../models/user')
var bcrypt = require('bcrypt')
var jwt = require('jwt-simple')
var config = require('../../config')

router.post('/', function(req, res, next) {

	User.findOne({$or: [{username: req.body.username}, {email: req.body.username}]})
	.select('password').select('username').select('token_data').select('_id')
	.exec(function(err, user) {
		if(err) {
			return next(err)
		}
		if (!user) {
			return res.send({status: "fail"})
		}

		bcrypt.compare(req.body.password, user.password, function(err, valid) {
			if (err) {
				return next(err)
			}
			if (!valid) {
				return res.send({status: "fail"})
			}
			var token = jwt.encode({_id: user._id, token_data: user.token_data.toString()}, config.secret)
			User.update({_id: user._id}, {last_login: new Date()}, function(err) {

				if (err) {
					return next(err)
				}
				res.send({status: "success", token: token})
			})
		})
	})
})

module.exports = router