var router = require('express').Router()
var bcrypt = require('bcrypt')
var jwt = require('jwt-simple')
var User = require('../../../../models/user')
var Friends = require('../../../../models/friends')
var Activity = require('../../../../models/activity')
var config = require('../../../../config')

router.post('/add_edit_activity', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}

	
	var activityObj = {};
	if (req.body.activity._id) {
		activityObj._id = req.body.activity._id
	}
	activityObj.what = req.body.activity.what;
    activityObj.how = req.body.activity.how;
    activityObj.isDone = req.body.activity.isDone;
    activityObj.visibility = req.body.activity.visibility;

	activityObj.owner = req.auth._id;

	if (!activityObj.what || activityObj.what == '') {
		res.send({status: 'validationError'})
		return
	}

    if (!activityObj.how) {
		activityObj.how = '';
	}

	activityObj.lastModified = new Date()

	if (req.body.isCreate) {
		activityObj.dateCreated = activityObj.lastModified; 
		var activity = new Activity(activityObj)

		activity.save(function(err, a) {
			if (err) {
				return next(err)
			}
			res.send({status: "success", id: a._id})
		})
	} else {
		var id = activityObj._id
		delete activityObj._id;
		Activity.update({_id: id},
			activityObj,
			null,
			function(err) {
				if (err) {
					return next(err)
				}
				res.send({status: "success", lastModified: activityObj.lastModified})
			})
	}


})

router.post('/delete_activity', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}

	// 

	Activity.remove({_id: req.body.id, owner: req.auth._id}, function(err) {
		if (err) {
			return next(err)
		}
		res.send({status: "success"});
	})
	


})

router.post('/find_watched_activities', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	var query = null;

	if (!req.body.lastDateModified) {
		query = {
			watchIds: req.auth._id
		}
	} else {
		query = {
			$and:[ {
				watchIds: req.auth._id
			},
			{
				lastModified: {
					$lt: req.body.lastDateModified
				}
			}
			]
		}
		
	}

	Activity.find(query, null, 
		{
			skip:0,
			limit:10, 
			sort: {lastModified: -1}
		}
	).populate('owner')
	.exec(function (err, activities) {
		if (err) {
				return next(err)
			}
		res.json({activities: activities})
	});

})

router.post('/find_last_activities', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	
	Friends.find({$or: [{id1: req.auth._id}, {id2: req.auth._id}]},
				{}, {}, function(err, friends) {
		if (err) {
			return next(err)
		}

		var friendsIds = []
		friends.forEach(function(e) {
			if (e.status == 'friends') {
				if (e.id1 == req.auth._id) {
					friendsIds.push(e.id2)
				} else {
					friendsIds.push(e.id1)
				}
			}
		})
		
		query = 

		{
			owner: {$in: req.authUser.watchIds},

			$or: [
				{
					watchIds: req.auth._id
				},
				{
					owner: { $in: friendsIds },
					visibility: {$ne: 'Private'}
				},
				{
					owner: { $nin: friendsIds },
					visibility: 'Public'
				}
			]

		}

		if (req.body.lastDateModified) {
			query.lastModified = { $lt: req.body.lastDateModified }
		}


		Activity.find(query, null, 
			{
				skip:0,
				limit:10, 
				sort: {lastModified: -1}
			}
		).populate('owner')
		.exec(function (err, activities) {
			if (err) {
					return next(err)
				}
			res.json({activities: activities})
		});


	})

})

router.post('/find', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	var query = null;


	if (req.body.lastDateModified) {
		query = { $and: [
					{
						lastModified: {
							$lt: req.body.lastDateModified
						}
					},
					{
						owner: req.body.seeingUser
					}]

			}
	} else {
		query = { $and: [
					{
						owner: req.body.seeingUser
					}]

			}
	}

	var myId = req.auth._id;

	Friends.findOne({$or: [{id1: req.body.seeingUser, id2: myId }, {id1: myId, id2: req.body.seeingUser}]})
		.exec(function (err, user) {
		if (err) {
			return next(err)
		}
	
		if (myId == req.body.seeingUser) {
			// ok
			if (!req.body.lastDateModified) {
				query = {
							owner: req.body.seeingUser
						};
			}
		} else if (!user) {
			query.$and.push({
				visibility: 'Public'
			})
		} else if (user) {
			query.$and.push({
				$or: [
					{visibility: 'Public'},
					{visibility: 'All friends'}
				]
			})
		}

		if (!req.body.lastDateModified) {
			query = {$or: [{
				watchIds: req.auth._id, owner: req.body.seeingUser
			},
			query]
			}
		} else {
			query = {$or: [
			{
				$and:[ {
					watchIds: req.auth._id
				},
				{
					lastModified: {
						$lt: req.body.lastDateModified
					}
				},
				{
					owner: req.body.seeingUser
				}
				]
			},
			query
				]}

			
		}


		Activity.find(query, null, 
			{
				skip:0,
				limit:10, 
				sort: {lastModified: -1}
			}).populate('owner').exec(
			function(err, activities) {
				if (err) {
					return next(err)
				}
				res.json({activities: activities})
			}
		)

	})


})

router.post('/good_luck', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	var activityObj;
	if (req.body.op) {
		activityObj = {$addToSet: {goodLuckIds: req.auth._id}}
	} else {
		activityObj = {$pull: {goodLuckIds: req.auth._id}}
	}

	Activity.update({_id: req.body.id},
			activityObj,
			null,
			function(err) {
				if (err) {
					return next(err)
				}
				res.send('ok')
			})
})

router.post('/congrats', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	var activityObj;
	if (req.body.op) {
		activityObj = {$addToSet: {congratsIds: req.auth._id}}
	} else {
		activityObj = {$pull: {congratsIds: req.auth._id}}
	}

	Activity.update({_id: req.body.id},
			activityObj,
			null,
			function(err) {
				if (err) {
					return next(err)
				}
				res.send('ok')
			})
})

router.post('/watch', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}

	var activityObj;
	if (req.body.op) {
		activityObj = {$addToSet: {watchIds: req.auth._id}}
	} else {
		activityObj = {$pull: {watchIds: req.auth._id}}
	}

	Activity.update({_id: req.body.id},
			activityObj,
			null,
			function(err) {
				if (err) {
					return next(err)
				}
				res.send('ok')
			})
})

module.exports = router