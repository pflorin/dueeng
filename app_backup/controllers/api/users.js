var router = require('express').Router()
var bcrypt = require('bcrypt')
var jwt = require('jwt-simple')
var User = require('../../models/user')
var Friends = require('../../models/friends')
var config = require('../../config')

router.get('/', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	User.findOne({_id: req.auth._id})
		.exec(function (err, user) {
		if (err) {
			return next(err)
		}
		User.update({_id: user._id}, {last_activity: new Date()}, function(err){
			if (err) {
				return next(err)
			}
			res.json(user)
		})
	})
})

router.post('/register', function(req, res, next) {

	var user = new User({
		email: req.body.email,
		username: req.body.username,
		first_name: req.body.first_name,
		last_name: req.body.last_name,
		date_created: new Date(),
		token_data: new Date()
	})
	bcrypt.hash(req.body.password, 10, function(err, hash) {
		if (err) {
			return next(err)
		}
		user.password = hash
		user.save(function(err) {
			if (err) {
				if (err.code = 11000 && err.message.indexOf('username') != -1) {
					res.send("duplicate_username")
				}
				if (err.code = 11000 && err.message.indexOf('email') != -1) {
					res.send("duplicate_email")
				}
				return next(err)
			}
			res.send("success")
		})
	})
})

router.post('/change_password', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}

	User.findOne({_id: req.auth._id})
	.select('password').select('_id')
	.exec(function(err, user) {
		if(err) {
			return next(err)
		}
		if (!user) {
			return res.send(401)
		}

		bcrypt.compare(req.body.old_password, user.password, function(err, valid) {
			if (err) {
				return next(err)
			}
			if (!valid) {
				return res.send({status: "fail"})
			}
			bcrypt.hash(req.body.new_password, 10, function(err, hash) {
				if (err) {
					return next(err)
				}
				var date = new Date();
				var token = jwt.encode({_id: user._id, token_data: date.toString()}, config.secret)
				User.update({_id: user._id}, {password: hash, date_password_changed: new Date(), token_data: date}, function(err) {
					if(err) {
						return next(err)
					}
					res.send({status: "success", token: token})
				})
			})
		})
	})
})

router.post('/update_account_info', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}

	User.update({_id: req.auth._id}, {username: req.body.username, first_name: req.body.first_name, last_name: req.body.last_name, date_account_changed: new Date()}, function(err) {
		if (err) {
			if (err.code = 11000 && err.message.indexOf('username') != -1) {
				res.send("duplicate_username")
			}
			return next(err)
		}
		res.send({status: "success"})
	})
})

router.post('/update_about', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}

	User.update({_id: req.auth._id}, {about: req.body.about}, function(err) {
		if (err) {
			return next(err)
		}
		res.send({status: "success"})
	})
})


router.post('/logout', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	User.findOne({_id: req.auth._id})
		.exec(function (err, user) {
		if (err) {
			return next(err)
		}
		User.update({_id: user._id}, {date_logged_out: new Date()}, function(){

		})
		res.json("success")
	})

})

router.post('/get_user_by_username', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	User.findOne({username: req.body.username})
		.exec(function (err, user) {
		if (err) {
			return next(err)
		}
		res.json(user)
	})

})

router.post('/get_users', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	var keys = req.body.searchString.split(' ')
	var keywordsRegexes = []

	keys.forEach(function(key) {
		keywordsRegexes.push(new RegExp(key, 'i'))
	});

	Friends.find({$and: [{$or: [{id1: req.auth._id}, {id2: req.auth._id}]}, {status: 'friends'}]},
							{}, {}, function(err, friends) {
					if (err) {
						return next(err)
					}

					var friendsIds = []
					friends.forEach(function(e) {
						if (e.id1 == req.auth._id) {
							friendsIds.push(e.id2)
						} else {
							friendsIds.push(e.id1)
						}
					})
					User.find({ $and: [
									{ $or: [
										{first_name: { $in: keywordsRegexes}},
										{last_name: { $in: keywordsRegexes}},
										{username: { $in: keywordsRegexes}}
									]}
									,
									{
										_id: {
											$in: friendsIds
										}
									},
									{
						                _id: {
						                    $nin: req.body.withoutIds
						                }

									},
									{
										_id: {
											$ne: req.auth._id
										}
									}]

							}, null, 
							{
								skip:0,
				    			limit:8, 
								sort: {first_name: 1}
							},
							function(err, users) {
								if (err) {
									return next(err)
								}

								res.json(users)
					})
					
	})


})




module.exports = router