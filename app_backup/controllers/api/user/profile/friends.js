var router = require('express').Router()
var bcrypt = require('bcrypt')
var jwt = require('jwt-simple')
var User = require('../../../../models/user')
var Friends = require('../../../../models/friends')
var config = require('../../../../config')

router.post('/find_people', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	var keys = req.body.keywords.split(' ')
	var keywordsRegexes = []

	keys.forEach(function(key) {
		keywordsRegexes.push(new RegExp(key, 'i'))
	});

	User.find({ $and: [
					{ $or: [
						{first_name: { $in: keywordsRegexes}},
						{last_name: { $in: keywordsRegexes}},
						{username: { $in: keywordsRegexes}}
					]}
					,
					{
						username: {
							$gt: req.body.lastUsername
						}
					},
					{
						_id: {
							$ne: req.auth._id
						}
					}]

			}, null, 
			{
				skip:0,
    			limit:15, 
				sort: {username: 1}
			},
			function(err, users) {

				if (err) {
					return next(err)
				}
				Friends.find({$or: [{id1: req.auth._id}, {id2: req.auth._id}]},
							{}, {}, function(err, friends) {
					if (err) {
						return next(err)
					}

					res.json({users: users, friends: friends})
				})
			})


})


router.post('/get_watching_for_profile', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	var keys = req.body.keywords.split(' ')
	var keywordsRegexes = []

	keys.forEach(function(key) {
		keywordsRegexes.push(new RegExp(key, 'i'))
	});

	var queryObj = { $and: [
						{ $or: [
							{first_name: { $in: keywordsRegexes}},
							{last_name: { $in: keywordsRegexes}},
							{username: { $in: keywordsRegexes}}
						]}
						,
						{
							username: {
								$gt: req.body.lastUsername
							}
						},
						{
							_id: {
								$in: req.body.watchersIds
							}
						}]

	}

	if (req.auth._id == req.body.seeingUserId) {
			queryObj.$and[2]._id.$ne = req.auth._id
	}


	User.find(queryObj, null, 
			{
				skip:0,
    			limit:15, 
				sort: {username: 1}
			},
			function(err, users) {

				if (err) {
					return next(err)
				}
				Friends.find({$or: [{id1: req.auth._id}, {id2: req.auth._id}]},
							{}, {}, function(err, friends) {
					if (err) {
						return next(err)
					}

					res.json({users: users, friends: friends})
				})
			})


})

router.post('/get_friends_for_profile', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	var keys = req.body.keywords.split(' ')
	var keywordsRegexes = []

	keys.forEach(function(key) {
		keywordsRegexes.push(new RegExp(key, 'i'))
	});

	Friends.find({$or: [{id1: req.body.seeingUserId}, {id2: req.body.seeingUserId}]},
							{}, {}, function(err, friends) {
		if (err) {
			return next(err)
		}
		var friendsIds = []
		friends.forEach(function(e) {
			if (e.status == 'friends') {
				if (e.id1 == req.body.seeingUserId) {
					friendsIds.push(e.id2)
				} else {
					friendsIds.push(e.id1)
				}
			}
		})

		var queryObj = { $and: [
							{ $or: [
								{first_name: { $in: keywordsRegexes}},
								{last_name: { $in: keywordsRegexes}},
								{username: { $in: keywordsRegexes}}
							]}
							,
							{
								username: {
									$gt: req.body.lastUsername
								}
							},
							{
								_id: {
									$in: friendsIds
								}
							}]

		}

		if (req.auth._id == req.body.seeingUserId) {
				queryObj.$and[2]._id.$ne = req.auth._id
		}

		User.find(queryObj, null, 
				{
					skip:0,
	    			limit:15, 
					sort: {username: 1}
				},
				function(err, users) {

					if (err) {
						return next(err)
					}
					Friends.find({$or: [{id1: req.auth._id}, {id2: req.auth._id}]},
								{}, {}, function(err, friends) {
						if (err) {
							return next(err)
						}

						res.json({users: users, friends: friends})
					})
		})
	})


})

router.post('/get_friends', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	Friends.find({$or: [{id1: req.auth._id}, {id2: req.auth._id}]},
				{}, {}, function(err, friends) {
		if (err) {
			return next(err)
		}
		res.json(friends)
	})
})

router.post('/watch', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}

	User.update({_id: req.auth._id}, {$addToSet: {watchings: {id: req.body.id, date_created: new Date()}}}, function(err) {
		if(err) {
			return next(err)
		}
		return res.send('ok')
	})
})

router.post('/unwatch', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}
	User.update({_id: req.auth._id}, {$pull: {watchings: {id: req.body.id}}}, function(err) {
		if(err) {
			return next(err)
		}
		return res.send('ok')
	})
})


router.post('/send_friend_request', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}

	var friends = new Friends({
		id1: req.body.id1,
		id2: req.body.id2,
		status: 'pending',
		initiator: req.auth._id,
		date_friended: null,
		date_created: new Date()
	})
	
	friends.save(function(err, f) {
		if (err) {
			return next(err)
		}
		res.send({status: "ok", _id: f._id})
	})
})

router.post('/undo_send_friend_request', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}

	Friends.remove({ _id: req.body.id }, function(err) {
	    if (err) {
	        return next(err)
	    }
	    return res.send('ok')
	})
})

router.post('/accept_friend_request', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}

	Friends.update({_id: req.body.id}, {status: 'friends', date_friended: new Date()}, function(err) {
		if(err) {
			return next(err)
		}
		return res.send('ok')
	})
})

router.post('/unfriend', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}

	Friends.remove({ _id: req.body.id }, function(err) {
	    if (err) {
	        return next(err)
	    }
	    return res.send('ok')
	})
})


module.exports = router