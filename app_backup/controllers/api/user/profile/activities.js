var router = require('express').Router()
var bcrypt = require('bcrypt')
var jwt = require('jwt-simple')
var User = require('../../../../models/user')
var Friends = require('../../../../models/friends')
var Activity = require('../../../../models/activity')
var config = require('../../../../config')

router.post('/add_activity', function(req, res, next) {
	if (!req.headers['x-auth']) {
		return res.send(401)
	}

	
	var activityObj = req.body.activity

	if (!activityObj.title || activityObj.title == '') {
		res.send('validationError')
		return
	}

    if (activityObj.visibility == 'Select friends'
        && !activityObj.visibilityIds.length) {
    	res.send('validationError')
		return
    }

    if (!activityObj.title || activityObj.title == '') {
		res.send('validationError')
		return
	}

	activityObj.dateCreated = new Date()
	activityObj.lastModified = new Date()
	activityObj.comments = []
	activityObj.goodLucksIds = []
	activityObj.congratsIds = []
	activityObj.pendingParticipantsIds = []
	activityObj.invitedParticipantsIds = []

	var activity = new Activity(activityObj)

	activity.save(function(err) {
		if (err) {
			return next(err)
		}
		res.send("success")
	})

})

module.exports = router