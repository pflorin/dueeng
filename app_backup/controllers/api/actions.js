var Action = require('../../models/action')
var router = require('express').Router()
var pubsub = require('../../pubsub')

router.get('/', function(req, res, next) {
	Action.find()
	.sort('-date')
	.exec(function(err, actions) {
		if (err) {
			return next(err)
		}
		res.json(actions)
	})
})

router.post('/', function(req, res, next) {
	var action = new Action({
		body: req.body.body
	})

	if (req.auth) {
		action.username = req.auth.username
	} else {
		action.username = req.body.username
	}
	action.save(function(err, action) {
		if (err) {
			return next(err)
		}
		pubsub.publish('new_action', action)
		res.json(201, action)
	})
})

pubsub.subscribe('new_action', function(action) {
})

module.exports = router