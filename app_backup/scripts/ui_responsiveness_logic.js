function changeScreenLayout(newWidth, forced) {

	if (!elementsAreLoaded()) {
		return;
	}

	if (newWidth < 700) {


		if (!forced && window.layoutCase == "min") {
			return;
		}
		window.layoutCase = "min";

		storeScroll();
		// show left button
		document.getElementById("left-button").style.display = "block";
		// set panel-cell1 width to 100%
		document.getElementById("cell1").style.width = "100%";
		// hide panel-cell2
		document.getElementById("cell1").style.display = "none";

		document.getElementById("cell2").style.display = "table-cell";

		document.getElementById("main-view").style.width = "100%";
		document.getElementById("nav-bar").style.width = "95%";

		document.getElementById("left-button").innerHTML = "<b>Menu</b>";

		setLogoAndUserHeaderVisibility(false);

		
	} else {

		setLogoAndUserHeaderVisibility(true);
		
		/*if (window.layoutCase == "normal") {
			return;
		}*/
		window.layoutCase = "normal"

		// hide left button
		document.getElementById("left-button").style.display = "none";
		// set panel-cell1 width to 200px
		document.getElementById("cell1").style.display = "table-cell";
		document.getElementById("cell1").style.width = "200px";
		// show panel 2
		document.getElementById("cell2").style.display = "table-cell";

		document.getElementById("main-view").style.width = "850px";
		document.getElementById("nav-bar").style.width = "850px";

	}
}

function setLogoAndUserHeaderVisibility(visible) {
	var visibleLabel = "block";
	var visibleInlineLabel = "inline-block";
	var invisibleLabel = "none";
	if (!visible) {
		visibleLabel = "none";
		visibleInlineLabel = "none";
		invisibleLabel = "block";
	}
	document.getElementById("current-user").style.display = visibleLabel;
	document.getElementById("logo").style.display = visibleInlineLabel;
	document.getElementById("logo-2-current-user-2").style.display = invisibleLabel;
	//document.getElementById("logo-2").style.display = invisibleLabel;
}

function elementsAreLoaded() {

	if (
	document.getElementById("left-button") != null
	&& document.getElementById("logo-2") != null
	&& document.getElementById("logo") != null
	&& document.getElementById("current-user") != null
	&& document.getElementById("current-user-2") != null 
	&& document.getElementById("logo-2-current-user-2") != null
	&& document.getElementById("cell1") != null
	&& document.getElementById("cell2") != null
	&& document.getElementById("nav-bar") != null
	&& document.getElementById("panel2") != null
	) {
		return true;
	}
	return false;
}
/*
window.onload = function() {
	changeScreenLayout(window.innerWidth);
}
*/

window.onresize = function(event) {
	changeScreenLayout(window.innerWidth);
}

function leftButtonClickHandler() {
	var cell2 = document.getElementById("cell2");
	if (cell2.style.display == "none") {
		document.getElementById("cell1").style.display = "none";
		document.getElementById("cell2").style.display = "table-cell";
		document.getElementById("left-button").innerHTML = "<b>Menu</b>";
		window.scrollTo(0, window.myScroll);
	} else {
		storeScroll();
		document.getElementById("cell1").style.display = "table-cell";
		document.getElementById("cell2").style.display = "none";
		document.getElementById("left-button").innerHTML = "<b>View</b>";
	}
}

function storeScroll() {
	var doc = document.documentElement;
	window.myScroll = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
}
