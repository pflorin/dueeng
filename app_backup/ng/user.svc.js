angular.module('app')
.service('UserSvc', function($http) {
	var svc = this
	svc.getUser = function() {
		return $http.get('/api/users', {
			headers: {'X-Auth': $http.defaults.headers.common['X-Auth']},
		})
	}

	svc.restoreLogin = function(token) {
		$http.defaults.headers.common['X-Auth'] = token
		return svc.getUser()
	}

	svc.getUserByUsername = function(username) {
		return $http.post('/api/users/get_user_by_username', {
			username: username
		}).then(function(val) {
			return val.data

		}, function(error) {
			return ""
		})
	}

	svc.getUsers = function(searchString, withoutIds) {
		return $http.post('/api/users/get_users', {
			searchString: searchString,
			withoutIds: withoutIds
		})
	}

	svc.login = function(username, password, checkbox) {
		return $http.post('/api/sessions', {
			username: username, password: password
		}).then(function(val) {
			if (val.data.status == "success") {
				if (checkbox) {
					window.localStorage.token = val.data.token
				} else {
					document.cookie = 'token=' + val.data.token;
				}
				$http.defaults.headers.common['X-Auth'] = val.data.token
				return {status: 'success'}
			} else {
				return {status: 'fail'}
			}

		}, function(error) {
			return null
		})
	}

	svc.register = function(email, username, password, first_name, last_name) {
		return $http.post('/api/users/register', {
			email: email,
			username: username, 
			password: password,
			first_name: first_name,
			last_name: last_name
		}).then(function(val) {
			return val.data
		}, function(error) {
			return null
		})
	}

	svc.changePassword = function(old_password, new_password) {
		return $http.post('/api/users/change_password', {
			old_password: old_password,
			new_password: new_password
		}).then(function(val) {
			// restore new token
			if (val.data && val.data.status == 'success') {
				if (window.localStorage.token && window.localStorage.token != 'null') {
					window.localStorage.token = val.data.token
				}
				if (document.cookie != '' && document.cookie && document.cookie.length != "token=".length) {
					document.cookie = 'token=' + val.data.token;
				}
				$http.defaults.headers.common['X-Auth'] = val.data.token
			}
			return val.data;
		}, function(error) {
			return null
		})
	}

	svc.updateAccountInfo = function(username, first_name, last_name) {
		return $http.post('/api/users/update_account_info', {
			username: username,
			first_name: first_name, 
			last_name: last_name
		}).then(function(val) {
			return val.data;
		}, function(error) {
			return null
		})
	}
	
	svc.updateAbout = function(about) {
		return $http.post('/api/users/update_about', {
			about: about
		}).then(function(val) {
			return val.data;
		}, function(error) {
			return null
		})
	}

	svc.logout = function() {
		$http.post('/api/users/logout')
	}
})