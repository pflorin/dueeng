angular.module('app')
.service('ActionsSvc', function($http) {
	this.fetch = function() {
		return $http.get('/api/actions')
	}

	this.create = function(action) {
		return $http.post('/api/actions', action)
	}

	this.logout = function() {
		window.localStorage.token = null
		$http.defaults.headers.common['X-Auth'] = undefined
		return;
	}
})