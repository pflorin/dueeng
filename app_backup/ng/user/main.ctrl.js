angular.module('app').controller('MainCtrl', function($rootScope, $scope, $state, authUser, FriendsSvc) {
	if (!authUser) {
		$scope.$emit('invalid_auth')
		return;
	}

	if (authUser == 'invalid_auth') {
		$scope.$emit('invalid_auth')
		return;
	}

	$rootScope.currentUser = authUser

	$scope.goHome = function() {
		$state.go('main.watch', {}, {reload: true});
	}

	$scope.goToFindPeople = function() {
		$state.go('main.findPeople', {}, {reload: true});
	}

	$scope.goToMyProfile = function() {
		$state.go('main.activities', {username: $rootScope.currentUser.username}, {reload: true});
	}

	$scope.openFriend = function(user) {
		$state.go('main.activities', {username: user.username}, {reload: true})
	}



	// friends, knowing him/her/it?, friend request sent, respond to friend request,
	$scope.getFriendshipStatus = function(user) {
		var message = "knowing him/her/it?"
		$rootScope.friends.forEach(function(e) {
			if (e.id1 == user._id || e.id2 == user._id) {
				if (e.status == 'friends') {
					message = 'friends'
				} else {
					if (e.initiator != user._id) {
						message = 'friend request sent'
					} else {
						message = 'respond to friend request'
					}
				}
			}
		})
		return message;
	}

	// watching, interesting?
	$scope.getWatchingStatus = function(user) {
		var message = 'interesting?'
		$rootScope.currentUser.watchings.forEach(function(e) {
			if (e.id == user._id) {
				message = 'watching'
			}
		})
		return message
	}

	// add friend, undo add friend, accept friend request, unfriend
	$scope.getFriendship = function(user) {
		var message = "add friend"
		$rootScope.friends.forEach(function(e) {
			if (e.id1 == user._id || e.id2 == user._id) {
				if (e.status == 'friends') {
					message = 'unfriend'
				} else {
					if (e.initiator != user._id) {
						message = 'undo add friend'
					} else {
						message = 'accept friend request'
					}
				}
			}
		})
		return message
	}

	// watch, unwatch
	$scope.getWatching = function(user) {
		var message = 'watch him/her/it'
		$rootScope.currentUser.watchings.forEach(function(e) {
			if (e.id == user._id) {
				message = 'unwatch him/her/it';
			}
		})
		return message
	}

	$scope.addFriendship = function(user) {
		user.loadingF = true;
		user.hasFriendError = false;
		user.friendErrorMessage = 'try again / hit refresh'
		var v = $rootScope.friends
		for (var i = 0; i < v.length; i++) {
			var e = v[i]
			if (e.id1 == user._id || e.id2 == user._id) {
				if (e.status == 'friends') {
					return FriendsSvc.unfriend(e._id)
					.then(function(val) {
						if (val.data == 'invalid_auth') {
							$scope.$emit('invalid_auth')
							return;
						}
						if (val.data == 'ok') {
							$rootScope.friends.splice(i, 1);
						} else {
							user.hasFriendError = true;
						}
						user.loadingF = false;
					}, function(err) {
						user.hasFriendError = true;
						user.loadingF = false;
					})
				} else {
					if (e.initiator != user._id) {
						return FriendsSvc.undoSendFriendRequest(e._id)
						.then(function(val) {
							if (val.data == 'invalid_auth') {
								$scope.$emit('invalid_auth')
								return;
							}
							if (val.data == 'ok') {
								$rootScope.friends.splice(i, 1);
							} else {
								user.hasFriendError = true;
							}
							user.loadingF = false;
						}, function(err) {
							user.hasFriendError = true;
							user.loadingF = false;
						})
					} else {
						return FriendsSvc.acceptFriendRequest(e._id)
						.then(function(val) {
							if (val.data == 'invalid_auth') {
								$scope.$emit('invalid_auth')
								return;
							}
							if (val.data == 'ok') {
								$rootScope.friends[i].status = 'friends'
							} else {
								user.hasFriendError = true;
							}
							user.loadingF = false;
						}, function(err) {
							user.hasFriendError = true;
							user.loadingF = false;
						})

					}
				}
			}
		}
		var id1 = user._id
		var id2 = $rootScope.currentUser._id
		if (id1 > id2) {
			var id1 = $rootScope.currentUser._id
			var id2 = user._id
		}
		return FriendsSvc.sendFriendRequest(id1, id2)
		.then(function(val) {
			if (val.data && val.data == 'invalid_auth') {
				$scope.$emit('invalid_auth')
				return;
			}
			if (val.data && val.data.status == 'ok') {
				$rootScope.friends.push({
					_id: val.data._id,
					id1: id1,
					id2: id2,
					status: 'pending',
					initiator: $rootScope.currentUser._id
				})
			} else {
				user.hasFriendError = true;
			}
			user.loadingF = false;
		}, function(err) {
			user.hasFriendError = true;
			user.loadingF = false;
		})
	}

	$scope.addWatching = function(user) {
		user.loadingW = true;
		user.hasWatchError = false;
		user.watchErrorMessage = 'try again / hit refresh'
		var v = $rootScope.currentUser.watchings
		for (var i = 0; i < v.length; i++) {
			var e = v[i]
			if (e.id == user._id) {
				return FriendsSvc.unwatch(user._id)
				.then(function(val) {
					if (val.data == 'invalid_auth') {
						$scope.$emit('invalid_auth')
						return;
					}
					if (val.data == 'ok') {
						$rootScope.currentUser.watchings.splice(i, 1)
					} else {
						user.hasWatchError = true;
					}
					user.loadingW = false;
				}, function(err) {
					user.hasWatchError = true;
					user.loadingW = false;
				})
			}
		}
		return FriendsSvc.watch(user._id)
		.then(function(val) {
			if (val.data == 'invalid_auth') {
				$scope.$emit('invalid_auth')
				return;
			}
			if (val.data == 'ok') {
				$rootScope.currentUser.watchings.push({id: user._id})
			} else {
				user.hasWatchError = true;
			}
			user.loadingW = false;
		}, function(err) {
			user.hasWatchError = true;
			user.loadingW = false;
		})
	}
})