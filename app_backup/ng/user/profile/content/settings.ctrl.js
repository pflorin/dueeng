angular.module('app')
.controller('SettingsCtrl', function($scope, $rootScope, $location, UserSvc) {
	$scope.hasError = false
	$scope.hasSuccess = false
	$scope.loading = false
	$scope.changePassword = function(old_password, new_password, re_new_password) {

		$scope.hasError = false
		$scope.hasSuccess = false

		if (!old_password || old_password == '' 
			|| !new_password || new_password == ''
			|| !re_new_password || re_new_password == '') {
			$scope.hasError = true
			$scope.errorMessage = "Please fill in the fields."
			return;	
		}

		if (new_password != re_new_password) {
			$scope.hasError = true
			$scope.errorMessage = "The last two passwords must be the same."
			return;
		}

		if (new_password == old_password) {
			$scope.hasError = true
			$scope.errorMessage = "The new password should not be the same."
			return;
		}
		$scope.loading = true
		UserSvc.changePassword(old_password, new_password)
		.then(function(response) {
			$scope.loading = false
			if (response) {
				if (response == 'invalid_auth') {
					$scope.$emit('invalid_auth')
					return;
				}
				if (response.status == 'success') {
					$scope.hasSuccess = true;
					$scope.old_password = ''
					$scope.new_password = ''
					$scope.re_new_password = ''
					$scope.successMessage = "Password successfully changed!"
				} else {
					$scope.hasError = true
					$scope.errorMessage = "Old password must match your current password."
				}
			} else {
				$scope.hasError = true
				$scope.errorMessage = "Error. Please try again."
			}
		})
	}


	$scope.hasAccountError = false
	$scope.hasAccountSuccess = false
	$scope.loadingAccount = false

	$scope.username = $rootScope.currentUser.username
	$scope.first_name = $rootScope.currentUser.first_name	
	$scope.last_name = $rootScope.currentUser.last_name

	$scope.updateAccountInfo = function(username, first_name, last_name) {

		function isUsernameValid(username) {
			var re = /^[0-9a-zA-Z_.]+$/g;
			return re.test(username);
		}

		$scope.hasAccountError = false
		$scope.hasAccountSuccess = false

		if (!username || username == '' 
			|| !first_name || first_name == ''
			|| !last_name || last_name == '') {
			$scope.hasAccountError = true
			$scope.accountErrorMessage = "Please fill in the fields."
			return;	
		}

		if (!isUsernameValid(username)) {
			$scope.hasAccountError = true
			$scope.accountErrorMessage = "Username must consist of letters, digits, '_', and '.'"
			return;
		}

		$scope.loadingAccount = true
		UserSvc.updateAccountInfo(username, first_name, last_name)
		.then(function(response) {
			$scope.loadingAccount = false
			if (response) {
				if (response == 'invalid_auth') {
					$scope.$emit('invalid_auth')
					return;
				}
				if (response.status == 'success') {
					$scope.hasAccountSuccess = true;
					
					$rootScope.currentUser.username = $scope.username
					$rootScope.currentUser.first_name = $scope.first_name	
					$rootScope.currentUser.last_name = $scope.last_name

					$scope.accountSuccessMessage = "Account successfully updated!"
				} else {
					$scope.hasAccountError = true
					$scope.accountErrorMessage = "Error. Username already exists"
				}
			} else {
				$scope.hasAccountError = true
				$scope.accountErrorMessage = "Error. Please try again."
			}
		})
	}
})