angular.module('app').controller('WatchCtrl', function($rootScope, $scope, $state, $stateParams) {

	$scope.startLoadingActivities = true;

	$scope.loadMore = false
	$scope.hasError = false
	$scope.loading = false
	$scope.searchedActivities = []
	//$rootScope.friends = []
	$scope.keywords = ""
	$scope.errorMessage = "There was an error. Please try again!"

	$scope.search = function(keywords) {
		$scope.searchedActivities = []
		$scope.loading = true
		$scope.hasError = false
		$scope.loadMore = false
		var lastUsername = ""
		$scope.keywords = keywords
		/*FriendsSvc.find($scope.currentState, keywords, lastUsername, $rootScope.seeingUser)
		.then(function(response) {
			$scope.loading = false
			if (response) {
				if (response == 'invalid_auth') {
					$scope.$emit('invalid_auth')
					return;
				}
				$rootScope.activities = response.activities
				$scope.searchedActivities = response.users
				if (response.users.length == 15) {
					$scope.loadMore = true
				} else {
					$scope.loadMore = false
				}
			} else {
				$scope.hasError = true
			}
		})*/
	}

	$scope.loadMoreActivities = function() {
		$scope.loading = true
		$scope.hasError = false
		if (!$scope.searchedActivities.length) {
			return;
		}
		var lastUsername = $scope.searchedActivities[$scope.searchedActivities.length - 1].username
		/*FriendsSvc.find($scope.currentState, $scope.keywords, lastUsername, $stateParams.username)
		.then(function(response) {
			$scope.loading = false
			if (response) {
				if (response == 'invalid_auth') {
					$scope.$emit('invalid_auth')
					return;
				}
				$rootScope.activities = response.activities
				$scope.searchedActivities = $scope.searchedActivities.concat(response.users)
				if (response.users.length == 15) {
					$scope.loadMore = true
				} else {
					$scope.loadMore = false
				}
			} else {
				$scope.hasError = true
			}
		})*/
	}

	if ($scope.startLoadingActivities) {
		$scope.search(' ')
	}

})