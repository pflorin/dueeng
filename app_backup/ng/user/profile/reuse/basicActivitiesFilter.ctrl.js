angular.module('app').controller('BasicActivitiesFilterCtrl', function($rootScope, $scope, $state, $stateParams, UserSvc) {

	$scope.showFilters = true;
	$scope.showAdvanced = true;	

	$scope.filterSelectOption = 'Scheduled';
	$scope.filterSelectOptions = ['Scheduled', 'Unscheduled']

	$scope.fromSelectOptions = ['Oldest', 'Today', 'Date', 'Newest']
	$scope.fromSelectOption = $scope.fromSelectOptions[1]

	$scope.format = 'dd-MM-yyyy'

	$scope.dt = new Date()

	$scope.status = {openedDate: false}

	$scope.openDate = function($event) {
		$scope.status.openedDate = true;
	};

	$scope.toSelectOption = 'Future';
	$scope.toSelectOptions = ['Past', 'Future']

	$scope.seeSelectOptions = ['Watching', 'All friends', 'Select friends']
	$scope.seeSelectOption = $scope.seeSelectOptions[0]

	$scope.whoWillSeeItems = []

	$scope.onSelectWhoWillSee = function ($item, $model, $label) {
	$scope.whoWillSeeItems.push($item)
	$scope.whoWillSee = ""
	}

	$scope.removeWhoWillSee = function(user) {
	for (var i = 0; i < $scope.whoWillSeeItems.length; i++) {
	  var e = $scope.whoWillSeeItems[i]
	  if (e._id == user._id) {
	    $scope.whoWillSeeItems.splice(i, 1)
	    break;
	  }
	}
	}

	$scope.getWhoWillSee = function(searchString) {
	var withoutIds = []
	$scope.whoWillSeeItems.forEach(function(e) {
	  withoutIds.push(e._id)
	})
	return $scope.getFriends(searchString, withoutIds)
	}

	$scope.getFriends = function(searchString, withoutIds) {
	return UserSvc.getUsers(searchString, withoutIds)
	      .then(function(val) {
	        if (val && val.data) {
	          return val.data
	        }
	        return []
	      }, function(err) {
	        return []
	      })
	}

})