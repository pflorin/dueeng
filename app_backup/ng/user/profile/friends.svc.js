angular.module('app')
.service('FriendsSvc', function($http) {
	var svc = this

	svc.find = function(state, keywords, lastUsername, seeingUser) {
		if (state == 'main.findPeople') {
			return svc.findPeople(keywords, lastUsername);
		} else if (state == 'main.watching') {
			return svc.getWatchingForProfile(keywords, lastUsername, seeingUser);
		} else if (state == 'main.friends') {
			return svc.getFriendsForProfile(keywords, lastUsername, seeingUser);
		}
	}

	svc.findPeople = function(keywords, lastUsername) {
		return $http.post('/api/friends/find_people', {
			keywords: keywords,
			lastUsername: lastUsername
		}).then(function(val) {
			return val.data
		}, function(error) {
			return null
		})
	}

	svc.getWatchingForProfile = function(keywords, lastUsername, seeingUser) {
		var watchersIds = []
		seeingUser.watchings.forEach(function(e) {
			watchersIds.push(e.id)
		})
		return $http.post('/api/friends/get_watching_for_profile', {
			keywords: keywords,
			lastUsername: lastUsername,
			seeingUserId: seeingUser._id,
			watchersIds: watchersIds
		}).then(function(val) {
			return val.data
		}, function(error) {
			return null
		})
	}

	svc.getFriendsForProfile = function(keywords, lastUsername, seeingUser) {
		return $http.post('/api/friends/get_friends_for_profile', {
			keywords: keywords,
			lastUsername: lastUsername,
			seeingUserId: seeingUser._id
		}).then(function(val) {
			return val.data
		}, function(error) {
			return null
		})
	}

	svc.getFriends = function() {
		return $http.post('/api/friends/get_friends', {
			data: ''
		}).then(function(val) {
			return val.data
		}, function(error) {
			return null
		})
	}

	svc.watch = function(id) {
		return $http.post('/api/friends/watch', {
			id: id
		})
	}

	svc.unwatch = function(id) {
		return $http.post('/api/friends/unwatch', {
			id: id
		})
	}

	svc.sendFriendRequest = function(id1, id2) {
		return $http.post('/api/friends/send_friend_request', {
			id1: id1,
			id2: id2
		})
	}

	svc.undoSendFriendRequest = function(id) {
		return $http.post('/api/friends/undo_send_friend_request', {
			id: id
		})
	}

	svc.acceptFriendRequest = function(id) {
		return $http.post('/api/friends/accept_friend_request', {
			id: id
		})
	}

	svc.unfriend = function(id) {
		return $http.post('/api/friends/unfriend', {
			id: id
		})
	}
})