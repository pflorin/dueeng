angular.module('app').controller('creteEditActivityCtrl', function ($rootScope, $scope, $modal, $log) {

  $rootScope.createEditActivity = function (isCreate) {
    
    var modalInstance = $modal.open({
      animation: false,
      templateUrl: './user/profile/create_edit_activity.html',
      controller: 'ModalInstanceCtrl',
      resolve: {
        items: function () {
          return $scope.items
        },
        isCreate: function() {
          return isCreate
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
    }, function () {
    });
  };

});

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

angular.module('app').controller('ModalInstanceCtrl', function ($rootScope, $scope, $modalInstance, items, isCreate, UserSvc, ActivitiesSvc) {

  $scope.isCreate = isCreate

  /*$scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };*/

  $scope.validate = function() {
    if (!$scope.title || $scope.title == '') {
      $scope.hasError = true
      $scope.errorMessage = 'Please fill the Title.'
      return false;
    }

    if ($scope.seeSelectOption == 'Select friends'
        && !$scope.whoWillSeeItems.length) {
      $scope.errorMessage = 'Please select at least one friend on "Who will see" part'
      $scope.hasError = true
      return false
    }

    return true

  }

  $scope.isSaving = false
  $scope.hasError = false
  $scope.hasSuccess = false
  $scope.save = function () {
    $scope.isSaving = true
    $scope.hasError = false
    $scope.hasSuccess = false
    if (!$scope.validate()) {
      $scope.isSaving = false
      return
    }

    var visibilityIds = []
    $scope.whoWillSeeItems.forEach(function(e) {
      visibilityIds.push(e._id)
    })

    var participantsIds = []
    $scope.participantsItems.forEach(function(e) {
      participantsIds.push(e._id)
    })

    var rightsIds = []
    $scope.rightsItems.forEach(function(e) {
      rightsIds.push(e._id)
    })

    addObj = {
      hasStart: $scope.startSelectOption != 'None',
      dateStart: $scope.startSelectOption != 'None' ? $scope.dtStart : null,
      hasStop: $scope.stopSelectOption != 'None',
      dateStop: $scope.stopSelectOption != 'None' ? $scope.dtStop : null,
      title: $scope.title,
      info: $scope.info,
      isDone: $scope.isDone,
      isJoinable: $scope.isJoinable,
      owner: $rootScope.currentUser._id,
      visibility: $scope.seeSelectOption,
      visibilityIds: visibilityIds,
      participantsIds: participantsIds,
      rightsIds: rightsIds
    }

    ActivitiesSvc.addActivity(addObj)
    .then(function(val) {
      if (val && val.data) {
        $scope.isSaving = false
        if (val.data == 'validationError') {
          $scope.hasError = true
          $scope.errorMessage = 'Plase try again or hit refresh.'
          return
        }

        if (val.data == 'success') {
          $scope.hasSuccess = true
          $scope.successMessage = 'Saved!'
          return
        }
        $scope.hasError = true
        $scope.errorMessage = 'Plase try again or hit refresh.'
        return 
      }
      
    }, function(err) {
      $scope.isSaving = false
      $scope.hasError = true
      $scope.errorMessage = 'Plase try again or hit refresh.'
    })

  };

  $scope.close = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.title = '';

  $scope.startSelectOptions = ['None', 'Date']
  $scope.startSelectOption = $scope.startSelectOptions[1]

  $scope.stopSelectOptions = ['None', 'Date']
  $scope.stopSelectOption = $scope.stopSelectOptions[1]
  $scope.format = 'dd-MM-yyyy'

  $scope.dtStart = new Date()
  $scope.dtStop = new Date()

  $scope.status = {openedStart: false, openedStop: false}

  $scope.openStart = function($event) {
    $scope.status.openedStart = true;
  };

  $scope.openStop = function($event) {
    $scope.status.openedStop = true;
  };

  $scope.isDone = false
  $scope.isJoinable = false


  $scope.seeSelectOptions = ['Public', 'All friends', 'Select friends', 'Private']
  $scope.seeSelectOption = $scope.seeSelectOptions[1]

  $scope.whoWillSeeItems = []

   $scope.onSelectWhoWillSee = function ($item, $model, $label) {
    $scope.whoWillSeeItems.push($item)
    $scope.whoWillSee = ""
  }

  $scope.removeWhoWillSee = function(user) {
    for (var i = 0; i < $scope.whoWillSeeItems.length; i++) {
      var e = $scope.whoWillSeeItems[i]
      if (e._id == user._id) {
        $scope.whoWillSeeItems.splice(i, 1)
        break;
      }
    }
  }

  $scope.getWhoWillSee = function(searchString) {
    var withoutIds = []
    $scope.whoWillSeeItems.forEach(function(e) {
      withoutIds.push(e._id)
    })
    return $scope.getFriends(searchString, withoutIds)
  }

  $scope.getFriends = function(searchString, withoutIds) {
    return UserSvc.getUsers(searchString, withoutIds)
          .then(function(val) {
            if (val && val.data) {
              return val.data
            }
            return []
          }, function(err) {
            return []
          })
  }






  $scope.participantsItems = []

   $scope.onSelectParticipants = function ($item, $model, $label) {
    $scope.participantsItems.push($item)
    $scope.participant = ""
  };

  $scope.removeParticipants = function(user) {
    for (var i = 0; i < $scope.participantsItems.length; i++) {
      var e = $scope.participantsItems[i]
      if (e._id == user._id) {
        $scope.participantsItems.splice(i, 1)
        break;
      }
    }
  }

  $scope.getParticipants = function(searchString) {
    var withoutIds = []
    $scope.participantsItems.forEach(function(e) {
      withoutIds.push(e._id)
    })
    return $scope.getFriends(searchString, withoutIds)
  }


  $scope.rightsItems = []

   $scope.onSelectRights = function ($item, $model, $label) {
    $scope.rightsItems.push($item)
    $scope.rightUser = ""
  };

  $scope.removeRights = function(user) {
    for (var i = 0; i < $scope.rightsItems.length; i++) {
      var e = $scope.rightsItems[i]
      if (e._id == user._id) {
        $scope.rightsItems.splice(i, 1)
        break;
      }
    }
  }

  $scope.getRights = function(searchString) {
    var withoutIds = []
    $scope.rightsItems.forEach(function(e) {
      withoutIds.push(e._id)
    })
    return $scope.getFriends(searchString, withoutIds)
  }

  $scope.showAdvanced = true


});