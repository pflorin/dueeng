angular.module('app')
.config(function($locationProvider, $stateProvider, $urlRouterProvider) {
	$locationProvider.html5Mode({
		enabled: true
	})
	$urlRouterProvider.otherwise("/");

	var regex = "[0-9a-zA-Z_.]+"; 

	$stateProvider

		.state('signup', {
			url: "/signup",
			templateUrl: "signup.html",
			controller: "SignupCtrl"
		})

		.state('signupSuccess', {
			url: "/signup_success",
			templateUrl: "signup_success.html"
		})

		.state('login', {
			url: "/login",
			templateUrl: "login.html",
			controller: "LoginCtrl"
		})

		.state('contact', {
			url: "/contact",
			templateUrl: "contact.html"
		})

		.state('forgotPassword', {
			url: "/forgot_password",
			templateUrl: "forgot_password.html"
		})

		.state('resetPassword', {
			url: "/reset_password",
			templateUrl: "reset_password.html"
		})

		.state('validateEmail', {
			url: "/:email/validate_email",
			templateUrl: "validate_email.html"
		})

		.state('info', {
			url: "/info",
			templateUrl: "info.html"
		})

		.state('terms', {
			url: "/terms",
			templateUrl: "terms.html"
		})

		.state('main', {
            templateUrl: "user/main.html",
            controller: "MainCtrl",
            resolve: {
            	authUser: ['UserSvc', function(UserSvc) {
            		function restoreLogin(token) {
						return UserSvc.restoreLogin(token)
						.then(function (user) {
							return user.data
						}, function(err) {
							//return 'invalid_auth'
							// not used because the error is catched in ApplicationCtrl: onstateerror
							// this is the same for all resolves
							return;
						})
					}
	            	if (window.localStorage.token && window.localStorage.token != 'null') {
						return restoreLogin(window.localStorage.token);
					}

					if (document.cookie != '' && document.cookie && document.cookie.length != "token=".length) {
						return restoreLogin(document.cookie.substring("token=".length, document.cookie.length))
					}
					window.location.href = '/login';
            		return null;

            	}]
            }
        })

        	.state('main.watch', {
				url: "/",
				views: {
					"header": { templateUrl: "user/header.html" },
					"menu": { templateUrl: "user/menu.html", controller: "ProfileMenuCtrl" },
					"content": { templateUrl: "user/watch.html", controller: "WatchCtrl" }
				},
				resolve: {
	            	seeingUser: function() {
	            		return ""
	            	},
	            	friends: function() {
	            		return ""
	            	}
	            }
			})

	    	.state('main.findPeople', {
				url: "/find_people",
				views: {
					"header": { templateUrl: "user/header.html" },
					"menu": { templateUrl: "user/menu.html", controller: "ProfileMenuCtrl" },
					"content": { templateUrl: "user/find_people.html",
								  controller: 'FindPeopleCtrl' }
				},
				resolve: {
	            	seeingUser: function() {
	            		return ""
	            	},
	            	friends: function() {
	            		return ""
	            	}
	            }
	    	})

	    	.state('main.activities', {
				url: "/{username:" + regex + "}/activities",
				views: {
					"header": { templateUrl: "user/header.html" },
					"menu": { templateUrl: "user/profile/profile_menu.html", controller: "ProfileMenuCtrl" },
					"content": { templateUrl: "user/profile/content/activities.html" }
				},
				resolve: {
	            	seeingUser: ['UserSvc', '$stateParams', function(UserSvc, $stateParams) {
	            		if (!$stateParams) {
	            			return ""
	            		}
	            		return UserSvc.getUserByUsername($stateParams.username)
							.then(function (user) {
								return user
							}, function(err) {
								return ""
							})
	            	}],
	            	friends: ['FriendsSvc', function(FriendsSvc) {
	            		return FriendsSvc.getFriends()
							.then(function (friends) {
								return friends
							}, function(err) {
								return null
							})
	            	}]
				}
	    	})
			.state('main.about', {
				url: "/{username:" + regex + "}/about",
				views: {
					"header": { templateUrl: "user/header.html" },
					"menu": { templateUrl: "user/profile/profile_menu.html", controller: "ProfileMenuCtrl" },
					"content": { templateUrl: "user/profile/content/about.html",
								controller: 'AboutCtrl' }
				},
				resolve: {
	            	seeingUser: ['UserSvc', '$stateParams', function(UserSvc, $stateParams) {
	            		if (!$stateParams) {
	            			return ""
	            		}
	            		return UserSvc.getUserByUsername($stateParams.username)
							.then(function (user) {
								return user
							}, function(err) {
								return ""
							})
	            	}],
	            	friends: ['FriendsSvc', function(FriendsSvc) {
	            		return FriendsSvc.getFriends()
							.then(function (friends) {
								return friends
							}, function(err) {
								return null
							})
	            	}]
				}
			})

			.state('main.activity', {
				url: "/{username:" + regex + "}/activity",
				views: {
					"header": { templateUrl: "user/header.html" },
					"menu": { templateUrl: "user/profile/profile_menu.html", controller: "ProfileMenuCtrl" },
					"content": { templateUrl: "user/profile/content/activity.html" }
				},
				resolve: {
	            	seeingUser: ['UserSvc', '$stateParams', function(UserSvc, $stateParams) {
	            		if (!$stateParams) {
	            			return ""
	            		}
	            		return UserSvc.getUserByUsername($stateParams.username)
							.then(function (user) {
								return user
							}, function(err) {
								return ""
							})
	            	}],
	            	friends: ['FriendsSvc', function(FriendsSvc) {
	            		return FriendsSvc.getFriends()
							.then(function (friends) {
								return friends
							}, function(err) {
								return null
							})
	            	}]
				}
			})

	    	.state('main.watching', {
				url: "/{username:" + regex + "}/watching",
				views: {
					"header": { templateUrl: "user/header.html" },
					"menu": { templateUrl: "user/profile/profile_menu.html", controller: "ProfileMenuCtrl" },
					"content": { templateUrl: "user/profile/content/watching.html",
								  controller: 'WatchingCtrl' }
				},
				resolve: {
	            	seeingUser: ['UserSvc', '$stateParams', function(UserSvc, $stateParams) {
	            		if (!$stateParams) {
	            			return ""
	            		}
	            		return UserSvc.getUserByUsername($stateParams.username)
							.then(function (user) {
								return user
							}, function(err) {
								return ""
							})
	            	}],
	            	friends: ['FriendsSvc', function(FriendsSvc) {
	            		return FriendsSvc.getFriends()
							.then(function (friends) {
								return friends
							}, function(err) {
								return null
							})
	            	}]
				}
	    	})

	    	.state('main.friends', {
				url: "/{username:" + regex + "}/friends",
				views: {
					"header": { templateUrl: "user/header.html" },
					"menu": { templateUrl: "user/profile/profile_menu.html", controller: "ProfileMenuCtrl" },
					"content": { templateUrl: "user/profile/content/friends.html",
								  controller: 'FriendsCtrl' }
				},
				resolve: {
	            	seeingUser: ['UserSvc', '$stateParams', function(UserSvc, $stateParams) {
	            		if (!$stateParams) {
	            			return ""
	            		}
	            		return UserSvc.getUserByUsername($stateParams.username)
							.then(function (user) {
								return user
							}, function(err) {
								return ""
							})
	            	}],
	            	friends: ['FriendsSvc', function(FriendsSvc) {
	            		return FriendsSvc.getFriends()
							.then(function (friends) {
								return friends
							}, function(err) {
								return null
							})
	            	}]
				}
	    	})

	    	.state('main.settings', {
				url: "/settings",
				views: {
					"header": { templateUrl: "user/header.html" },
					"menu": { templateUrl: "user/menu.html", controller: "ProfileMenuCtrl" },
					"content": { 
						templateUrl: "user/profile/content/settings.html",
						controller: "SettingsCtrl"
					}
				},
				resolve: {
	            	seeingUser: function() {
	            		return ""
	            	},
	            	friends: function() {
	            		return ""
	            	}
	            }
	    	})

	    	.state('main.help', {
				url: "/help",
				views: {
					"header": { templateUrl: "user/header.html" },
					"menu": { templateUrl: "user/menu.html", controller: "ProfileMenuCtrl" },
					"content": { templateUrl: "user/profile/content/help.html" }
				},
				resolve: {
	            	seeingUser: function() {
	            		return ""
	            	},
	            	friends: function() {
	            		return ""
	            	}
	            }
	    	})

	    	.state('main.theTerms', {
				url: "/the_terms",
				views: {
					"header": { templateUrl: "user/header.html" },
					"menu": { templateUrl: "user/menu.html", controller: "ProfileMenuCtrl" },
					"content": { templateUrl: "user/profile/content/terms.html" }
				},
				resolve: {
	            	seeingUser: function() {
	            		return ""
	            	},
	            	friends: function() {
	            		return ""
	            	}
	            }
	    	})

	    	.state('main.theInfo', {
				url: "/the_info",
				views: {
					"header": { templateUrl: "user/header.html" },
					"menu": { templateUrl: "user/menu.html", controller: "ProfileMenuCtrl" },
					"content": { templateUrl: "user/profile/content/info.html" }
				},
				resolve: {
	            	seeingUser: function() {
	            		return ""
	            	},
	            	friends: function() {
	            		return ""
	            	}
	            }
	    	})

	    	.state('main.theContact', {
				url: "/the_contact",
				views: {
					"header": { templateUrl: "user/header.html" },
					"menu": { templateUrl: "user/menu.html", controller: "ProfileMenuCtrl" },
					"content": { templateUrl: "user/profile/content/contact.html" }
				},
				resolve: {
	            	seeingUser: function() {
	            		return ""
	            	},
	            	friends: function() {
	            		return ""
	            	}
	            }
	    	})

})