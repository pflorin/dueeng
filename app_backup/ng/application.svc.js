angular.module('app')
.service('ApplicationSvc', function($http, UserSvc) {
	this.logout = function() {
		UserSvc.logout()
		window.localStorage.token = null
		document.cookie = 'token='
		$http.defaults.headers.common['X-Auth'] = undefined
		return;
	}
})