angular.module('app')
.controller('SignupCtrl', function($scope, UserSvc) {
	$scope.hasError = false
	$scope.loading = false
	$scope.signup = function(email, username, password, fistName, lastName) {

		function isEmailValid(email) {
			var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    		return re.test(email);
		}

		function isUsernameValid(username) {
			var re = /^[0-9a-zA-Z_.]+$/g;
			return re.test(username);
		}

		if (!email || email == '' 
			|| !username || username == '' 
			|| !password || password == ''
			|| !fistName || fistName == ''
			|| !lastName || lastName == '') {
			$scope.hasError = true
			$scope.errorMessage = "Please fill in the fields."
			return;	
		}

		if (!isEmailValid(email)) {
			$scope.hasError = true
			$scope.errorMessage = "Email is not valid."
			return;
		}

		if (!isUsernameValid(username)) {
			$scope.hasError = true
			$scope.errorMessage = "Username must consist of letters, digits, '_', and '.'"
			return;
		}
		$scope.loading = true
		UserSvc.register(email, username, password, fistName, lastName)
		.then(function(response) {

			if (response) {
				if (response == "success") {
					$scope.$emit('signup')
				} else if (response == "duplicate_username") {
					$scope.hasError = true
					$scope.errorMessage = "Username already exists."
					$scope.loading = false
				} else if (response == "duplicate_email") {
					$scope.hasError = true
					$scope.errorMessage = "Email already exists."
					$scope.loading = false
				}
			} else {
				$scope.hasError = true
				$scope.errorMessage = "An error occurred. Please try again."
				$scope.loading = false
			}
		})
	}
})