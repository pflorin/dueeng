angular.module('app').controller('ActionsCtrl', function($scope, ActionsSvc) {
	
	ActionsSvc.fetch().success(function(actions) {
		$scope.actions = actions
	})

	$scope.addAction = function() {

		if ($scope.actionBody) {

			ActionsSvc.create({
				username: 'anonimous',
				body: $scope.actionBody
			}).success(function(action) {
				// remove unshift because you are already using it above on "ws:new_action"
				//$scope.actions.unshift(action)
				$scope.actionBody = null
			})
		}

	}

	$scope.logout = function() {
		ActionsSvc.logout()
		$scope.$emit('logout')
	}
})