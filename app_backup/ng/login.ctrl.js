angular.module('app')
.controller('LoginCtrl', function($scope, UserSvc) {

	function getDefaultLoginText() {
		return 'Login'
	}

	$scope.checkbox = true // default value
	$scope.hasError = false
	$scope.loading = false
	$scope.login = function(username, password, checkbox) {

		$scope.hasError = false;

		if (!username || username == '' || !password || password == '') {
			$scope.hasError = true
			$scope.errorMessage = "Please fill in the fields."
			return;	
		}
		$scope.loading = true
		UserSvc.login(username, password, checkbox)
		.then(function(response) {
			if (response) {
				if (response.status == 'success') {
						$scope.$emit('login')
					
				} else {
					$scope.hasError = true
					$scope.errorMessage = "Invalid credentials."
					$scope.loading = false
				}
			} else {
				$scope.hasError = true
				$scope.errorMessage = "Error. Please try again."
				$scope.loading = false
			}
			
		})
	}
})