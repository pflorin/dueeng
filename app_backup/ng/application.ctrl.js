angular.module('app')
.controller('ApplicationCtrl', function($rootScope, $scope, $window, $http, $location, $state, UserSvc, ApplicationSvc) {

	$scope.appLoaded = true
	$scope.currentYear = new Date().getFullYear();

	/*$scope.restoreLogin = function(token) {
		return UserSvc.restoreLogin(token)
		.then(function (user) {
			$scope.currentUser = user.data 
		})
	}*/

	/*if (window.localStorage.token && window.localStorage.token != 'null') {
		restoreLogin(window.localStorage.token);
	}

	if (document.cookie != '' && document.cookie && document.cookie.length != "token=".length) {
		restoreLogin(document.cookie.substring("token=".length, document.cookie.length))
	}*/

	function cookieExists() {
		return (window.localStorage.token && window.localStorage.token != 'null') 
		 || (document.cookie != '' && document.cookie && document.cookie.length != "token=".length);
	}

	$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
		if (cookieExists()
			&& (
			 !toState.name.indexOf('signupSuccess')
			 || !toState.name.indexOf('signup')
			 || !toState.name.indexOf('login')
			 || !toState.name.indexOf('terms')
			 || !toState.name.indexOf('info')
			 || !toState.name.indexOf('contact')
			 ) ){
			event.preventDefault()
			window.location.href = '/';
			return;
		}

		if (!toState.name.indexOf('main')) {
			$scope.appLoaded = false
		}
	})

	$scope.$on('$stateChangeSuccess',
		function(event, toState, toParams, fromState, fromParams) {
			if (!toState.name.indexOf('main')) {
				$scope.appLoaded = true
			}
		$scope.currentState = toState.name
	})

	$scope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams){
		if (!toState.name.indexOf('main')) {
			//$scope.$emit('invalid_auth')
			return;
		}
		window.location.href = '/login'
	})

	$scope.$on('login', function(_) {
		window.location.href = '/'
	})

	$scope.$on('invalid_auth', function(_) {
		$rootScope.currentUser = null
		window.localStorage.token = null
		document.cookie = 'token='
		$http.defaults.headers.common['X-Auth'] = undefined
		window.location.href = '/login'
	})

	$scope.$on('signup', function(_) {
		$state.go('signupSuccess', {});
	})


	$scope.reload = function() {
		$state.go($scope.currentState, {}, {reload: true});
	}

	$scope.logout = function() {
		ApplicationSvc.logout()
		$rootScope.currentUser = null
		window.location.href = '/login'
	}

	$rootScope.isMobile = false
    $scope.updateIsMobile = function() {
    	if (window.innerWidth < 700) {
			$rootScope.isMobile = true
		} else {
			$rootScope.isMobile = false
		}
    }

	// hack for responsiveness to work
	$scope.$on('$viewContentLoaded', 
		function(event) {
    		changeScreenLayout(window.innerWidth, true);
    		$scope.updateIsMobile()
		}
	)

	angular.element($window).bind('resize', function() {
		$scope.updateIsMobile()
		return $scope.$apply();
    });

})