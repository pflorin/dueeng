var jwt = require('jwt-simple')
var config = require('./config')
var User = require('./models/user')

module.exports = function(req, res, next) {
	if (req.headers['x-auth'] && req.url.indexOf('.html', req.url.length - '.html'.length) === -1) {
		var auth = jwt.decode(req.headers['x-auth'], config.secret)
		User.findOne({_id: auth._id})
			.select('token_data')
			.exec(function (err, user) {
				if (err) {
					return next(err)
				}
				if (!user) {
					res.send('invalid_auth')
					return;
				}
				if (user.token_data.toString() !== auth.token_data.toString()) {
					res.send('invalid_auth')
					return;
				} else {
					req.auth = auth
					next()
				}
		})
		
	} else {
		next()
	}
}