
var gulp = require('gulp')
var concat = require('gulp-concat')
var minifyCss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var stylus = require('gulp-stylus')

gulp.task('css-stylus', function() {
	return gulp.src(['css/**/*.styl'])
	//.pipe(concat('app.css'))
	.pipe(stylus())
	.pipe(gulp.dest('css'))
})

gulp.task('css', ['css-stylus'], function() {
	gulp.src(['css/**/*.css'])
	.pipe(sourcemaps.init())
	.pipe(concat('app.css'))
	.pipe(minifyCss())
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('assets'))

})

// Problem here: if .styl file gets modified then css task will run twice
gulp.task('watch:css', ['css'], function() {
	gulp.watch('css/**/*.styl', ['css'])
	gulp.watch('css/**/*.css', ['css'])
})