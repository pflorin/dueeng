// test git
var gulp = require('gulp')
var concat = require('gulp-concat')
var uglify = require('gulp-uglify')
var ngAnnotate = require('gulp-ng-annotate')
var sourcemaps = require('gulp-sourcemaps')


gulp.task('js', function() {
	gulp.src(['ng/module.js', 'ng/**/*.js', 'scripts/**/*.js'])
	.pipe(sourcemaps.init())
		.pipe(concat('app.js'))
		.pipe(ngAnnotate())
		.pipe(uglify())
	.pipe(sourcemaps.write())
	.pipe(gulp.dest('assets'))
})

gulp.task('resources', function() {
	gulp.src(['resources/**/*'])
	.pipe(gulp.dest('assets'))
})

gulp.task('watch:js', ['js', 'resources'], function() {
	gulp.watch('ng/**/*.js', ['js'])
	gulp.watch('scripts/**/*.js', ['js'])
	gulp.watch('resources/**/*', ['resources'])
})
