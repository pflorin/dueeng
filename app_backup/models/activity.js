var db = require('../db')

var comments = db.Schema({
	id: 		    {type: db.Schema.ObjectId},
	dateCreated:    {type: Date},
	comment:    	{type: String}
})

var activity = db.Schema({
	dateCreated:            {type: Date, index: true},
	lastModified:           {type: Date},
	hasStart:               {type: Boolean},
	dateStart:              {type: Date, index: true},
	hasStop:                {type: Boolean},
	dateStop:               {type: Date, index: true},
	title:                  {type: String},
	info:                   {type: String},
	isDone:                 {type: Boolean},
	isJoinable:             {type: Boolean},
	comments:               [comments],
	goodLucksIds:           [db.Schema.ObjectId],
	congratsIds:            [db.Schema.ObjectId],
	owner:                  {type: db.Schema.ObjectId},
	visibility:             {type: String},
	visibilityIds:          [db.Schema.ObjectId],
	participantsIds:        [db.Schema.ObjectId],
	pendingParticipantsIds: [db.Schema.ObjectId],
	invitedParticipantsIds: [db.Schema.ObjectId],
	rightsIds:              [db.Schema.ObjectId]
})

module.exports = db.model('Activity', activity)