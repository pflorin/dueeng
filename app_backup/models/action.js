var db = require('../db')
var Action = db.model('Action', {
	username: {type: String, required: true},
	body: {type: String, required: true},
	date: {type: Date, required: true, default: Date.now}
})

module.exports = Action