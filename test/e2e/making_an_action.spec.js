var db = require('../../db')
var chai = require('chai')
chai.use(require('chai-as-promised'))
var expect = chai.expect

describe('making an action', function() {
	it('logs in and creates a new action', function() {
		// go to homepage
		browser.get('http://localhost:3001')

		// do things like element(by.css('form .btn')).click()
		// click 'login'
		// fill out and submit login form
		// submit a new action on the posts page

		// the user should now see their action as the first action on the page

		// use randomness

		// test with somwething like:
		/*element.all(by.css('ul.list-group li')).first().getText().p then(function (text) {
			expect(text).to.contain(post)
		})*/

		// or

		/*expect(element.all(by.css('ul.list-group li')).first().getText()).p to.eventually.contain(post)*/
	})
	afterEach(function() {
		db.connection.db.dropDatabase()
	})
})