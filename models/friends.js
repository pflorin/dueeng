var db = require('../db')
var friends = db.Schema({
	id1: 		     {type: db.Schema.ObjectId},
	id2: 		     {type: db.Schema.ObjectId},
	status:          {type: String}, // pending, friends
	initiator:       {type: db.Schema.ObjectId},
	date_friended:   {type: Date},
	date_created:    {type: Date}
})
friends.index({ id1: 1, id2: 1 }, { unique: true });

module.exports = db.model('Friends', friends)