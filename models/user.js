var db = require('../db')
var watching = db.Schema({
	id: 		     {type: db.Schema.ObjectId},
	date_created:    {type: Date}
})
var user = db.Schema({
	email:                 {type: String, required: true, index: {unique: true}, validate: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/},
	username:              {type: String, required: true, index: {unique: true}, validate: /^[0-9a-zA-Z_.]+$/},
	password:              {type: String, required: true, select: false},
	first_name: 		   {type: String, required: true},
	last_name: 			   {type: String, required: true},
	about:                 {type: String},
	date_created:          {type: Date},
	last_login:            {type: Date},
	last_activity:         {type: Date},
	date_password_changed: {type: Date},
	date_account_changed:  {type: Date},
	date_logged_out:       {type: Date},
	token_data:  		   {type: Date, select: false},
	watchIds:              [db.Schema.ObjectId]
})

module.exports = db.model('User', user)