var db = require('../db')

var activity = db.Schema({
	dateCreated:            {type: Date, index: {unique: true}},
	lastModified:           {type: Date, index: {unique: true}},
	what:                   {type: String},
	how:                    {type: String},
	isDone: 				{type: Boolean},
	owner:                  {type: db.Schema.ObjectId, ref: 'User' },
	visibility:             {type: String},
	goodLuckIds:            [db.Schema.ObjectId],
	congratsIds:            [db.Schema.ObjectId],
	watchIds:               [db.Schema.ObjectId]
})

module.exports = db.model('Activity', activity)